# Whats-Now
A simple appointment scheduler

![Logo Image](./frontend/public/img/icons/512.png)

## Features
- Modular application

### Backend
- `API` JSON RESTful
- High availability **Database**

### Frontend
- RealTime edition
- Auto completion
- High performance
- Simple use

## Technologies
- **Backend**:
	+ **Server**: [Python](https://www.python.org/) with [Flask](https://flask.palletsprojects.com/en/1.1.x/)
	+ **Database**: [MongoDB](https://www.mongodb.com/en)
- **Frontend**:
	- **JS Framework**: [VueJS](https://v3.vuejs.org/)
	- **CSS PreProcessor**: [Sass](https://sass-lang.com/)
