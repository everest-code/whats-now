from database.db import get_collection, mongo_apo as coll
from utils.conf import DATA as ENV
from utils.dates import create_delta, now as date_now

def delete_olders():
	max_time = date_now() - create_delta(months=ENV['configuration']['appointments']['remove_olders'])

	get_collection(coll).delete_many({
		"endAt": {
			"$lt": max_time,
		}
	})
