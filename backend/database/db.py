from pymongo import MongoClient, errors
from pymongo.collection import Collection
from utils.conf import DATA as env

mongo_conn = None
mongo_db = None

def get_collection(coll: str) -> Collection:
	global mongo_conn
	global mongo_db
	if mongo_db is None:
		mongo_conn = MongoClient(f"mongodb://{env['database']['host']}:{env['database']['port']}/")
		mongo_db = mongo_conn[env['database']['dbname']]

	return mongo_db[coll]

mongo_usr = 'users'
mongo_cli = 'clients'
mongo_apo = 'appointments'
