from jsonschema import Draft7Validator
from utils import ApiError
from utils.dates import DATETIME_REGEX


def simple_payload(val: dict = {}) -> dict:
	return {
		"type": "object",
		"required": ["payload"],
		"properties": {
			"payload": val
		},
	}


def users() -> dict:
	return {
		"type": "object",
		"required": ["nit", "fullName", "password"],
		"additionalProperties": False,
		"properties": {
			"nit": {
                "type": "integer",
                "exclusiveMinimum": 99999,
            },
			"fullName": {
                "type": "string",
                "pattern": r"^[a-z\\ ]+$",
            },
			"password": {
                "type": "string",
                "minLength": 6,
            },
		},
	}


def clients(modify: bool = False) -> dict:
	client_schema = {
		"type": "object",
		"additionalProperties": False,
		"properties": {
			"nitType": {
                "type": "string",
                "enum": ['TI', 'CC', 'CE'],
            },
			"nit": {
                "type": "integer",
                "exclusiveMinimum": 99999,
            },
			"email": {
                "type": "string",
                "pattern": r"^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$",
            },
			"phones": {
                "type": "array",
                "items": {
                    "type": "number",
                },
            },
			"names": {
				"type": "object",
				"required": ["firstName", "firstLastName"],
				"additionalProperties": False,
				"properties": {
					"firstName": {
                        "type": "string",
                    },
					"secondName": {
                        "type": "string",
                    },
					"firstLastName": {
                        "type": "string",
                    },
					"secondLastName": {
                        "type": "string",
                    },
				},
			},
		},
	}

	if not modify:
		client_schema['required'] = ["nitType", "nit", "names", "email", "phones"]
	else:
		del client_schema['properties']['nit']

	return client_schema


def appointments(modify: bool = False) -> dict:
	appointment_schema = {
		"type": "object",
		"additionalProperties": False,
		"properties": {
			"startAt": {
                "type": "string",
                "pattern": DATETIME_REGEX,
            },
			"endAt": {
                "type": "string",
                "pattern": DATETIME_REGEX,
            },
			"client": {
                "type": "number",
            },
			"description": {
                "type": "string",
            },
		},
	}

	if not modify:
		appointment_schema['required'] = ["startAt", "endAt", "client"]

	return appointment_schema


def validate(data, schema) -> dict:
	validator = Draft7Validator(schema)
	errors = list(validator.iter_errors(data))

	if len(errors) == 0:
		return None

	return ApiError(
		name="ValidationError",
		description=[
			{
				"message": error.message,
				"context": {
					"value": error.instance,
					"schema": error.schema,
					"path": '.'.join(error.absolute_path),
				},
			} for error in errors
		]
	)
