from flask import request, make_response, abort
from flask import jsonify as json
from bson import ObjectId
from utils import ApiBlueprint, ApiResponse, ApiError
from database.db import get_collection, mongo_apo, mongo_cli
from database.schemas import appointments as apo_schema
from database.schemas import validate
from utils.dates import parse_date, to_isodate, parse_isodate, parse_tz, datetime, create_delta, TZ_REGEX
import re, pymongo


def normalize(obj: dict) -> dict:
	obj['id'] = str(obj['_id'])
	del obj['_id']

	obj['startAt'] = to_isodate(obj['startAt'])
	obj['endAt'] = to_isodate(obj['endAt'])

	return obj


def is_available(fr: datetime, to: datetime) -> bool:
	res = get_collection(mongo_apo).find({
		'$or': [
			{
				'endAt': {
					'$gt': fr,
					'$lt': to,
				}
			},
			{
				'startAt': {
					'$gt': fr,
					'$lt': to,
				}
			}
		],
	})

	return len(list(res)) == 0

appointments_api = ApiBlueprint('appointments_api', __name__)


@appointments_api.route('/', methods = [ 'GET' ])
def get_appointment():
	reg = list(map(normalize, get_collection(mongo_apo).find().sort([('startAt', pymongo.DESCENDING)])))

	return make_response(ApiResponse(payload=reg))


@appointments_api.route('/byMonth/<int:year>-<int:month>', methods = [ 'GET' ])
def get_appointment_start_at(year: int, month: int):
	bottom = parse_date(year, month, 1)
	top = bottom + create_delta(days=31)

	bottom -= create_delta(days=bottom.day - 1)
	top -= create_delta(days=top.day - 1)

	reg = list(map(normalize, get_collection(mongo_apo).find({
		'startAt': {
			'$gte': bottom,
			'$lte': top
		}
	}).sort([('startAt', pymongo.DESCENDING)])))

	return make_response(ApiResponse(payload=reg))


@appointments_api.route('/', methods = [ 'POST' ])
def create_appointment():
	data = request.get_json()
	error = validate(data, apo_schema())

	if error is not None:
		return make_response(json(ApiResponse(
			status='error',
			error=error
		)), 400)

	if get_collection(mongo_cli).find_one({'nit': data['client']}) is None:
		return make_response(json(ApiResponse(
			status='error',
			error=ApiError(name='NotFound', description="Client not found")
		)), 404)

	data['startAt'] = parse_isodate(data['startAt'])
	data['endAt'] = parse_isodate(data['endAt'])

	if not is_available(data['startAt'], data['endAt']):
		return make_response(json(ApiResponse(
			status='error',
			error=ApiError(name='Unauthorized', description="The date is already used")
		)), 400)

	id = str(get_collection(mongo_apo).insert_one(data).inserted_id)
	return make_response(json(ApiResponse(payload=id)))


@appointments_api.route('/<appointment_id>', methods = [ 'PUT' ])
def modify_appointment(appointment_id: str):
	try:
		appointment_id = ObjectId(appointment_id)
	except Exception as e:
		return make_response(json(ApiResponse(
			status='error',
			error=ApiError.parse_exception(e)
		)), 400)

	apo = get_collection(mongo_apo).find_one({'_id': appointment_id})
	if apo is None:
		return make_response(json(ApiResponse(
			status='error',
			error=ApiError(
				name='NotFound',
				description=f'Appointment "{appointment_id}" not exist'
			)
		)), 404)

	start_at, end_at = apo['startAt'], apo['endAt']
	del apo

	data = request.get_json()
	error = validate(data, apo_schema(True))

	if error is not None:
		return make_response(json(ApiResponse(status='error', error=error)), 400)

	if 'client' in data:
		if get_collection(mongo_cli).find_one({'nit': data['client']}) is None:
			return make_response(json(ApiResponse(
				status='error',
				error=ApiError(name= 'NotFound', description="Client not found")
			)), 404)

	if 'startAt' in data:
		data['startAt'] = parse_isodate(data['startAt'])
		start_at = data['startAt']

	if 'endAt'in data:
		data['endAt'] = parse_isodate(data['endAt'])
		end_at = data['endAt']

	if not is_available(start_at, end_at):
		return make_response(json(ApiResponse(
			status='error',
			error=ApiError(description="The date is already used")
		)), 400)

	get_collection(mongo_apo).update_one({ '_id': appointment_id }, { '$set': data })
	return make_response(json(ApiResponse(payload='modified')))


@appointments_api.route('/<appointment_id>', methods = [ 'DELETE' ])
def delete_appointment(appointment_id: str):
	try:
		appointment_id = ObjectId(appointment_id)
	except Exception as e:
		return make_response(json(ApiResponse(
			status='error',
			error=ApiError.parse_exception(e)
		)), 400)

	if get_collection(mongo_apo).find_one({ '_id': appointment_id }) is None:
		return make_response(json(ApiResponse(
			status='error',
			error=ApiError(
				name='NotFound',
				description=f'Appointment "{appointment_id}" not exist'
			)
		)), 404)


	get_collection(mongo_apo).delete_one({ '_id': appointment_id })
	return make_response(json(ApiResponse(payload='deleted')))
