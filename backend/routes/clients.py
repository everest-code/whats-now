from flask import request, make_response
from flask import jsonify as json
from utils import ApiBlueprint, ApiResponse, ApiError
from database.db import mongo_cli, get_collection
from database.schemas import clients as cli_schema
from database.schemas import validate


def normalize(obj: dict) -> dict:
	del obj['_id']

	return obj

clients_api = ApiBlueprint('clients_api', __name__)


@clients_api.route('/', methods = [ 'GET' ])
def get_client():
	reg = list(map(normalize, get_collection(mongo_cli).find()))

	return make_response(ApiResponse(payload=reg))


@clients_api.route('/', methods = [ 'POST' ])
def create_client():
	data = request.get_json()
	error = validate(data, cli_schema())

	if error is not None:
		return make_response(json(ApiResponse(status='error', error=error)), 400)

	id = get_collection(mongo_cli).insert_one(data).inserted_id
	id = str(id)
	return make_response(json(ApiResponse(payload=id)))


@clients_api.route('/<int:nit>', methods = [ 'PUT' ])
def modify_client(nit: int):
	nit = int(nit)
	if get_collection(mongo_cli).find_one({'nit': nit}) is None:
		return make_response(json(ApiResponse(
			status='error',
			error=ApiError(
				name='NotFound',
				description=f'Client "{nit}" not exist'
			)
		)), 404)

	data = request.get_json()
	error = validate(data, cli_schema(True))

	if error is not None:
		return make_response(json(ApiResponse(status='error', error=error)), 400)

	get_collection(mongo_cli).update_one({ 'nit': nit }, { '$set': data })
	return make_response(json(ApiResponse(payload='modified')))
