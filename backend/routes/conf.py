from flask import make_response
from flask import jsonify as json
from database.db import mongo_usr, get_collection
from utils import ApiBlueprint, ApiResponse
from utils.conf import DATA as ENV

conf_api = ApiBlueprint('conf_api', __name__)

@conf_api.route('/userAuth', methods=['GET'])
def conf_user_auth():
	user_module = ENV['configuration']['user_auth']
	is_installed = False
	if len(list(get_collection(mongo_usr).find())) != 0 or not user_module:
		is_installed = True

	return make_response(json(ApiResponse(payload={
		'userModule': user_module,
		'isInstalled': is_installed,
	})))
