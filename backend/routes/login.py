from flask import request, make_response
from flask import jsonify as json
from utils import ApiBlueprint, ApiResponse, ApiError

from utils.crypt import crypt_password
from database.db import mongo_usr, get_collection
from utils.conf import DATA as ENV


def check_auth(user: str, passwd: str) -> str or None:
	usr = get_collection(mongo_usr).find_one({ 'nit': user, 'password': crypt_password(passwd) })
	if usr is None:
		return None

	return usr['fullName']

login_api = ApiBlueprint('login_api', __name__)


@login_api.route('/', methods = [ 'POST' ])
def api_login():
	auth = request.authorization
	if not auth or not auth.username or not auth.password:
		return make_response(json(ApiResponse(
			status='error',
			error=ApiError(name='ValidationError', description='Could verify')
		)), 401, {
			'WWW-Authenticate': 'Basic'
		})

	if ENV['configuration']['user_auth']:
		try:
			user = int(auth.username)
		except ValueError:
			return make_response(json(ApiResponse(
				status='error',
				error=ApiError(
					name='ParseError',
					description='Can\'t parse user to int'
				)
			)), 500)

		user_name = check_auth(user, auth.password)
		if user_name is None:
			return make_response(json(ApiResponse(
				status='error',
				error=[
					ApiError(
						name='CredentialError',
						description='Credentials not authorized'
					)
				]
			)), 403, {
				'WWW-Authenticate': 'Basic'
			})

		return make_response(json(ApiResponse(payload={
			'fullName': user_name,
			'user': user,
		})))

	return make_response(json(ApiResponse(payload={
		'fullName': 'usuario',
		'user': 0,
	})))
