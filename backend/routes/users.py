from flask import request, make_response
from flask import jsonify as json
from bson import ObjectId
from utils import ApiBlueprint, ApiResponse, ApiError
from utils.crypt import crypt_password
from database.db import mongo_usr, get_collection
from database.schemas import users as usr_schema, simple_payload
from database.schemas import validate


def normalize(obj: dict) -> dict:
	obj['_id'] = str(obj['_id'])

	return obj

users_api = ApiBlueprint('users_api', __name__)


@users_api.route('/', methods = [ 'GET' ])
def get_user():
	reg = list(map(normalize, get_collection(mongo_usr).find()))

	return make_response(ApiResponse(payload=reg))


@users_api.route('/', methods = [ 'POST' ])
def create_user():
	data = request.get_json()
	error = validate(data, usr_schema())

	if error is not None:
		return make_response(json(ApiResponse(status='error', error=error)), 400)

	if get_collection(mongo_usr).find_one({ 'nit': data['nit'] }) is not None:
		return make_response(json(ApiResponse(
			status='error',
			error=ApiError(
				name="MetaError",
				description="The user already exist"
			)
		)), 400)

	data['password'] = crypt_password(data['password'])

	get_collection(mongo_usr).insert_one(data)
	return make_response(json(ApiResponse(payload=str(data['nit']))))


@users_api.route('/<userId>/password', methods = [ 'PUT' ])
def modify_user_password(userId: str):
	try:
		userId = ObjectId(userId)
	except Exception as e:
		return make_response(json(ApiResponse(
			status='error',
			error=ApiError.parse_exception(e)
		)), 400)

	if get_collection(mongo_usr).find_one({'_id': userId}) is None:
		return make_response(json(ApiResponse(
			status='error',
			error=ApiError(
				name='NotFound',
				description=f'User "{userId}" not exist'
			)
		)), 404)

	data = request.get_json()
	error = validate(data, simple_payload({ "type": "string", "minLength": 6 }))

	if error is not None:
		return make_response(json(ApiResponse(status='error', error=error)), 400)

	data['payload'] = crypt_password(data['payload'])

	get_collection(mongo_usr).update_one({'_id': userId}, {'$set': {'password': data['payload']}})
	return make_response(json(ApiResponse(payload='modified')))


@users_api.route('/<userId>/fullName', methods = [ 'PUT' ])
def modify_user_full_name(userId: str):
	try:
		userId = ObjectId(userId)
	except Exception as e:
		return make_response(json(ApiResponse(status='error', error=ApiError.parse_exception(e))), 400)

	if get_collection(mongo_usr).find_one({'_id': userId}) is None:
		return make_response(json(ApiResponse(
			status='error',
			error=ApiError(
				name='NotFound',
				description=f'User \"{userId}\" not exist'
			)
		)), 404)

	data = request.get_json()
	error = validate(data, simple_payload({ "type": "string", "minLength": 6 }))

	if error is not None:
		return make_response(json(ApiResponse(status='error', error=error)), 400)

	get_collection(mongo_usr).update_one({'_id': userId}, {'$set': {'fullName': data['payload']}})
	return make_response(json(ApiResponse(payload='modified')))


@users_api.route('/<userId>', methods = [ 'DELETE' ])
def delete_user(userId: str):
	try:
		userId = ObjectId(userId)
	except Exception as e:
		return make_response(json(ApiResponse(status='error', error=ApiError.parse_exception(e))), 400)

	if get_collection(mongo_usr).find_one({'_id': userId}) is None:
		return make_response(json(ApiResponse(
			status='error',
			error=ApiError(
				name='NotFound',
				description=f'Appointment "{userId}" not exist'
			)
		)), 404)


	get_collection(mongo_usr).delete_one({ '_id': userId })
	return make_response(json(ApiResponse(payload='deleted')))
