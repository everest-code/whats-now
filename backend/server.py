#!/usr/bin/env python
# -*- coding: utf-8 -*-
# -*- author: sgt911 -*-
# -*- contributors: cdangel -*-

from flask import Flask
from flask_cors import CORS

from routes.login import login_api
from routes.appointments import appointments_api
from routes.clients import clients_api
from routes.conf import conf_api

from utils.conf import DATA as ENV
from utils.thread import set_interval

# Create Server
app = Flask(__name__)
CORS(app, resources={r"/*": {"origins": "*"}}) # Allow CORS in development mode

# Routing
if ENV['configuration']['user_auth']:
	from routes.users import users_api
	app.register_blueprint(users_api, url_prefix='/users')

app.register_blueprint(conf_api, url_prefix='/conf')
app.register_blueprint(login_api, url_prefix='/login')

app.register_blueprint(appointments_api, url_prefix='/appointments')
app.register_blueprint(clients_api, url_prefix='/clients')

# Background Tasks
if ENV['configuration']['appointments']['allow_remove']:
	from database.appointments import delete_olders
	set_interval(
		delete_olders,
		ENV['configuration']['appointments']['check_every'] * 60 * 60
	)

# Deploy Server
if __name__ == '__main__':
	app.run(debug=True)
