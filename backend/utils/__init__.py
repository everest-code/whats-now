from flask import Blueprint
from typing import Any, Optional, Callable


class ApiBlueprint(Blueprint):
	"""The Flask Blueprint for APIs."""

	def route(self, rule, **options):
		def decorator(f: Callable):
			new_rule = rule.rstrip('/')
			new_rule_with_slash = '{}/'.format(new_rule)
			super(ApiBlueprint, self).route(new_rule, **options)(f)
			super(ApiBlueprint, self).route(new_rule_with_slash, **options)(f)
			return f
		return decorator


class ApiError(dict):
	def __init__(self, name: str = 'Error', description: Any = None):
		super(ApiError, self).__init__({
			'name': name,
			'description': description
		})

	@staticmethod
	def parse_exception(ex: Exception) -> dict:
		description = ex.args
		if isinstance(description, tuple):
			description = ', '.join(description)

		return ApiError(
			name=ex.__class__.__name__,
			description=description,
		)


class ApiResponse(dict):
	def __init__(self, status: str = "ok", payload: Optional[Any] = None, error: Optional[ApiError] = None):
		super(ApiResponse, self).__init__({
			'status': status,
			'payload': payload,
			'error': error,
		})


def third_operator(condition: bool, if_yes: Any, if_no: Any) -> Any:
	if condition:
		return if_yes

	return if_no
