import toml
from os import environ as env

DATA = None
with open(env.get('CONF_FILE', './conf.toml'), 'r') as file:
	DATA = toml.loads(file.read())
	file.close()
