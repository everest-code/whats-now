import hashlib


def crypt_password(passwd: str) -> str:
	return hashlib.md5(passwd.encode()).hexdigest()
