from threading import Timer
from typing import Callable


def set_interval(func: Callable[[], None], secs: float = 1): 
	def func_wrapper(): 
		set_interval(func, secs)
		func() 

	t = Timer(secs, func_wrapper) 
	t.start() 

	return t 
