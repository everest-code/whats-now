import { request } from "../index"

export async function getAll() {
	let req = await request('/appointments')
		.enableCORS()
		.get()
		.send()
	
	return req.body.payload
}

export async function create(appointment) {
	let req = await request('/appointments')
	.enableCORS()
	.post()
	.addBody(appointment)
	.send()

	if (req.status == 200) {
		return req.body.payload
	}

	throw req.body.error
}

export async function update(id, appointment) {
	let req = await request(`/appointments/${id}`)
	.enableCORS()
	.put()
	.addBody(appointment)
	.send()

	if (req.status == 200) {
		return req.body.payload
	}

	throw req.body.error
}

export async function remove(id) {
	let req = await request(`/appointments/${id}`)
	.enableCORS()
	.delete()
	.send()

	if (req.status == 200) {
		return req.body.payload
	}

	throw req.body.error
}