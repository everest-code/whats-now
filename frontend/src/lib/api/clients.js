import { request } from "../index"

export async function getAll() {
	let req = await request('/clients')
		.enableCORS()
		.get()
		.send()
	
	return req.body.payload
}

export async function create(client) {
	let req = await request('/clients')
	.enableCORS()
	.post()
	.addBody(client)
	.send()

	if (req.status == 200) {
		return req.body.payload
	}

	throw req.body.error
}

export async function update(nit, client) {
	let req = await request(`/clients/${nit}`)
	.enableCORS()
	.put()
	.addBody(client)
	.send()

	if (req.status == 200) {
		return req.body.payload
	}

	throw req.body.error
}