import { request } from "../index"

export async function getUserModule() {
	const req = await request('/conf/userAuth')
		.enableCORS()
		.get()
		.send()
	return req.body.payload
}