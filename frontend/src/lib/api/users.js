import { request } from "../index"

export async function create(data) {
	const req = await request('/users')
	.enableCORS()
	.addBody(data)
	.post()
	.send()

	if (req.body.status != 'ok') {
		throw req.body.error
	}

	return await req.body.payload
}

export async function login({nit, password}) {
	const req = await request('/login')
	.enableCORS()
	.authenticate(nit.toString(), password)
	.post()
	.send()

	if (req.status == 403) {
		return null
	} else if (req.status == 200) {
		return req.body.payload
	}

	throw req.body.error
}