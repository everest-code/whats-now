import { fillZero } from '../lib'

export const months = [
	'Enero',
	'Febrero',
	'Marzo',
	'Abril',
	'Mayo',
	'Junio',
	'Julio',
	'Agosto',
	'Septiembre',
	'Octubre',
	'Noviembre',
	'Diciembre'
]

export const days = [
	'Domingo',
	'Lunes',
	'Martes',
	'Miercoles',
	'Jueves',
	'Viernes',
	'Sabado'
]

export function parseDate(date) {
	return `${days[date.getDay()]} ${date.getDate()} de ${months[date.getMonth()]} de ${date.getFullYear()}`
}

export function parseDateWithTime(date) {
	let hour = date.getHours()
	let minutes = date.getMinutes()
	let ampm = 'AM'

	if (hour > 12) {
		hour -= 12
		ampm = 'PM'
	}
	return `${fillZero(hour)}:${fillZero(minutes)} ${ampm}, ${parseDate(date)}`
}

export function stringToDate(date) {
	return new Date(...date.split('-').map(num => parseInt(num)))
}

export function dateToString(date) {
	let day = date.getDate()
	let month = date.getMonth() + 1
	let year = date.getFullYear()

	return `${year}-${fillZero(month)}-${fillZero(day)}`
}

export function dateTimeToString(date) {
	let hour = date.getHours()
	let minutes = date.getMinutes()

	return `${dateToString(date)}T${fillZero(hour)}:${fillZero(minutes)}`
}

export function today() {
	let date = new Date()

	date.setMilliseconds(0)
	date.setSeconds(0)
	date.setMinutes(0)
	date.setHours(0)

	return date
}

export function now() {
	let date = new Date()

	date.setMilliseconds(0)
	date.setSeconds(0)
	date.setMinutes(Math.round(date.getMinutes() / 5) * 5)

	return date
}

export function getMonthDays(year, month) {
	if (month == 2) {
		if (((year % 4 == 0) && (year % 100 != 0 )) || (year % 400 == 0)) return 29
		return 28
	}

	if ([1, 3, 5, 7, 8, 10, 12].indexOf(month) !== -1) return 31

	return 30
}