export const errorNames = {
	default: 'Error',
	data: 'DataError',
	notFound: 'NotFound',
}