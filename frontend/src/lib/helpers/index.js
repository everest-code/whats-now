import { watch } from 'vue'
import { capitalize, reduceInt } from '../parsing'

export function toNumeric(target) {
	watch(target, (value) => {
		if (value == null) return null
		
		value = reduceInt(value)

		target.value = value
	})
}

export function toCapital(target) {
	watch(target, (value) => {
		if (value == null) return null

		if (value == '') {
			value = null
		} else {
			value = capitalize(value)
		}

		target.value = value
	})
}

export function nullOnEmpty(target) {
	watch(target, (value) => {
		if (value == '') {
			value = null
		}

		target.value = value
	})
}