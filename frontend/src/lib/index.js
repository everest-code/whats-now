import { apiEndpoint } from '../config'

import UIkit from 'uikit'

export const fillZero = (num) => (num < 10)? `0${num}` : num.toString()

export function notify(data) {
	let { type, message, timeout } = {
		timeout: 3000,
		type: 'default',
		...data
	}

	return UIkit.notification({
		status: type,
		message,
		timeout,
		pos: 'bottom-right'
	})
}

export function sleep(ms) {
	return new Promise(resolve => setTimeout(resolve, ms));
}

export function request(url) {
	return new APIRequest(
		`${apiEndpoint}${url}`,
		'application/json',
		JSON.stringify
	)
}

class APIRequest {
	constructor(url, contentType, parser) {
		this.url = url
		this.headers = new Headers()
		this.method = 'GET'
		this.contentType = contentType
		this.parser = parser
		this.cors = false
		this.body = null
	}

	headers(headers) {
		for (const key in headers) {
			if (Object.hasOwnProperty.call(headers, key)) {
				const value = headers[key];
				this.headers.append(key, value)
			}
		}

		return this
	}

	enableCORS() {
		this.cors = true
		return this
	}

	addBody(data) {
		this.body = data
		this.headers.append('Content-Type', this.contentType)
		return this
	}

	changeMethod(method) {
		method = method.toUpperCase()
		if (['GET', 'POST', 'PUT', 'OPTIONS', 'DELETE'].includes(method)) {
			this.method = method
		} else {
			this.method = 'GET'
		}

		return this
	}

	authenticate(user, password) {
		const token = user + ":" + password
		const hash = btoa(token)

		this.headers.append('Authorization', `Basic ${hash}`)

		return this
	}

	get() {
		return this.changeMethod('GET')
	}

	post() {
		return this.changeMethod('POST')
	}

	put() {
		return this.changeMethod('PUT')
	}

	options() {
		return this.changeMethod('OPTIONS')
	}

	delete() {
		return this.changeMethod('DELETE')
	}

	async send() {
		const req = await fetch(this.url, {
			method: this.method,
			mode: (this.cors)? 'cors' : undefined,
			headers: this.headers,
			body: (this.body == null)? undefined : this.parser(this.body)
		})

		let body
		if (req.headers.get('Content-Type') == 'application/json') {
			body = await req.json()
		} else {
			body = await req.text()
		}

		return {
			body,
			status: req.status,
			headers: req.headers
		}
	}
}