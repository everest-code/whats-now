/**
 * @param {{
 * 	names: {
 * 		firstName: string;
 * 		secondName?: string;
 * 		firstLastName: string;
 * 		secondLastName?: string;
 * 	};
 * }} data 
 */
export function fullName(data) {
	let fullName = [ data.names.firstName ]
	if (data.names.secondName) {
		fullName.push(data.names.secondName)
	}

	fullName.push(data.names.firstLastName)

	if (data.names.secondLastName) {
		fullName.push(data.names.secondLastName)
	}

	return fullName.join(' ')
}

/**
 * @param {number} phone 
 */
export function phoneFormat(phone) {
	let phoneStr = phone.toString()

	// (313)769-2049
	return `(${phoneStr.substr(0, 3)})${phoneStr.substr(3, 3)}-${phoneStr.substr(6)}`
}