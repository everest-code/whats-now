export function capitalize(value) {
	return value.split(' ').map(el => {
		return el.substr(0, 1).toUpperCase() + el.substr(1).toLowerCase()
	}).join(' ')
}

export function reduceInt(value) {
	value = value.toString().split('').filter(el => /[0-9]/.test(el)).join('')
	if (value == '') {
		value = null
	} else {
		value = parseInt(value)
	}

	return value
}