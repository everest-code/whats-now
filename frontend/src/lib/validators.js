export const emailRegexp = /[a-z\_\-\+\.0-9]+\@[a-z\_\-\+\.0-9]+(\.[a-z]+)+/i
export const appointmentRegexp = /[a-f0-9]{24}/