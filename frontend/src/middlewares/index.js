export async function runMiddlewares(middlewares, data = {}) {
	for (let i = 0; i < middlewares.length; i++) {
		const middleware = middlewares[i];
		let res = middleware(data)
		if (res instanceof Promise) {
			res = await res
		}

		if (res == false) break
	}
}