export default async ({store, router}) => {
	const userModule = await store.getters['config/userModule']
	
	if (!userModule.isInstalled) {
		router.push('/install')
		return false
	}

	return true
}