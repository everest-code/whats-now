export default async ({router, store}) => {	
	const userModule = await store.getters['config/userModule']
	
	if (!store.getters['user/loggedIn'] && userModule.userModule) {
		router.push('/login')
		return false
	}

	return true
}