const titleElement = document.head.getElementsByTagName('title').item(0)
const titlePrefix = titleElement.innerText

export default function({ route }) {
	if (route.name != null || route.name != undefined) {
		titleElement.innerText = `${titlePrefix} | ${route.name}`
	} else {
		titleElement.innerText = titlePrefix
	}

	return true
}