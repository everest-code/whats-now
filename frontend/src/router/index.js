import { createRouter, createWebHashHistory } from 'vue-router'
import Home from '../views/Home.vue'
import Login from '../views/Login.vue'

const routes = [
  {
    path: '/',
    name: null,
    component: Home
  },
  {
    path: '/login',
    name: 'Iniciar Sesion',
    component: Login
  },
  {
    path: '/logout',
    name: 'Cerrar Sesion',
    component: () => import(/* webpackChunkName: "logout" */ '../views/Logout.vue')
  },
  {
    path: '/install',
    name: 'Instalacion',
    component: () => import(/* webpackChunkName: "install" */ '../views/Install.vue')
  },
  {
    path: '/clients',
    name: 'Clientes',
    component: () => import(/* webpackChunkName: "clients/get" */ '../views/clients/Get.vue')
  },
  {
    path: '/clients/set',
    name: 'Gestion Cliente',
    component: () => import(/* webpackChunkName: "clients/set" */ '../views/clients/Set.vue')
  },
  {
    path: '/appointments',
    name: 'Citas',
    component: () => import(/* webpackChunkName: "appointments/get" */ '../views/appointments/Get.vue')
  },
  {
    path: '/appointments/byDate/:date',
    name: 'Citas por Fecha',
    component: () => import(/* webpackChunkName: "appointments/getByDate" */ '../views/appointments/GetByDate.vue')
  },
  {
    path: '/appointments/set',
    name: 'Gestion Cita',
    component: () => import(/* webpackChunkName: "appointments/set" */ '../views/appointments/Set.vue')
  },
  {
    path: '/appointments/calendar',
    name: 'Calendario',
    component: () => import(/* webpackChunkName: "appointments/calendar" */ '../views/appointments/Calendar.vue')
  },
  {
    path: '/appointments/delete/:id([a-f0-9]{24})',
    name: 'Eliminar Cita',
    component: () => import(/* webpackChunkName: "appointments/delete" */ '../views/appointments/Delete.vue')
  },
  {
    path: '/credits',
    name: 'Copyrigth & Creditos',
    component: () => import(/* webpackChunkName: "credits" */ '../views/Credits.vue')
  },
  {
    path: '/:path(.*)*',
    name: 'No Encontrado',
    props: true,
    component: () => import(/* webpackChunkName: "errors/notFound" */ '../views/errors/NotFound.vue')
  },
  {
    path: '/errorHandler',
    name: 'Error',
    props: true,
    component: () => import(/* webpackChunkName: "errors/internal" */ '../views/errors/Internal.vue')
  }
]


export const appRoutes = [
  {
    path: '/',
    name: 'Inicio',
    icon: 'home'
  },
  {
    path: '/clients',
    name: 'Clientes',
    icon: 'people'
  },
  {
    path: '/appointments',
    name: 'Citas',
    icon: 'book'
  },
  {
    path: '/appointments/calendar',
    name: 'Calendario Citas',
    icon: 'calendar_today'
  }
]

const router = createRouter({
  history: createWebHashHistory(),
  routes
})

export default router
