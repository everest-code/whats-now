import * as apiAppointments from '../lib/api/appointments'
import { dateToString } from '../lib/dates'

export default {
	namespaced: true,
	state() {
		return {
			data: []
		}
	},
	mutations: {
		load(state, clients) {
			state.data = clients
		},
		clear(state) {
			state.data = new Array()
		},
		addOrUpdate(state, appointment) {
			for(let i = 0; i < state.data.length; i++) {
				if (state.data[i].id == appointment.id) {
					state.data[i] = {
						...state.data[i],
						...appointment
					}

					return
				}
			}

			state.data.push(appointment)
		},
		remove(state, id) {
			state.data = state.data.filter(el => el.id != id)
		}
	},
	actions: {
		async reload({commit, dispatch}) {
			commit('clear')
			await dispatch('load')
		},
		async load({commit, getters}) {
			if (getters['isEmpty']) {
				let data = await apiAppointments.getAll()
				commit('load', data.map(data => {
					return {
						...data,
						startAt: new Date(data.startAt),
						endAt: new Date(data.endAt)
					}
				}))
			}
		},
		async create({ commit }, appointment) {
			let id = await apiAppointments.create(appointment)
			commit('addOrUpdate', {
				...appointment,
				startAt: new Date(appointment.startAt.substr(0, 16)),
				endAt: new Date(appointment.endAt.substr(0, 16)),
				id
			})

			return id
		},
		async update({ commit }, {id, appointment}) {
			await apiAppointments.update(id, appointment)
			commit('addOrUpdate', {
				...appointment,
				startAt: new Date(appointment.startAt.substr(0, 16)),
				endAt: new Date(appointment.endAt.substr(0, 16)),
				id
			})
		},
		async remove({ commit }, id) {
			await apiAppointments.remove(id)
			commit('remove', id)
		}
	},
	getters: {
		isEmpty: (state) => state.data.length == 0,
		join: (state, getters, rootState, rootGetters) => {
			let clientMaps = rootGetters['clients/asMap']

			return state.data.map(el => {
				return {
					...el,
					client: {...clientMaps[el.client]}
				}
			})
		},
		asIDMap: (state, getters) => getters['join'].reduce((acc, el) => {
			acc[el.id] = el

			return acc
		}, {}),
		asMap: (state, getters) => getters['join'].reduce((acc, el) => {
			const key = dateToString(el.startAt)
		
			if (acc.hasOwnProperty(key)) {
				acc[key].push(el)
			} else {
				acc[key] = [el]
			}

			return acc
		}, {}),
		getDay: (state, getters) => (day) => getters['asMap'][day]?? [],
		getDayNumber: (state, getters) => (day) => getters['getDay'](day).length
	}
}

export const plugins = []