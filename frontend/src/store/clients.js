import * as apiClients from '../lib/api/clients'

export default {
	namespaced: true,
	state() {
		return {
			data: []
		}
	},
	mutations: {
		load(state, clients) {
			state.data = clients
		},
		clear(state) {
			state.data = new Array()
		},
		addOrUpdate(state, client) {
			for(let i = 0; i < state.data.length; i++) {
				if (state.data[i].nit == client.nit) {
					state.data[i] = {
						...state.data[i],
						...client
					}

					return
				}
			}

			state.data.push(client)
		}
	},
	actions: {
		async reload({commit, dispatch}) {
			commit('clear')
			await dispatch('load')
		},
		async load({commit, getters}) {
			if (getters['isEmpty']) {
				let data = await apiClients.getAll()
				commit('load', data)
			}
		},
		async create({ commit }, client) {
			let id = await apiClients.create(client)
			commit('addOrUpdate', {
				...client,
				_id: id
			})
		},
		async update({ commit }, {nit, client}) {
			await apiClients.update(nit, client)
			commit('addOrUpdate', {
				...client,
				nit
			})
		}
	},
	getters: {
		isEmpty: (state) => state.data.length == 0,
		asMap: (state) => state.data.reduce((acc, el) => {
			acc[el.nit] = el
			return acc
		}, {})
	}
}

export const plugins = []