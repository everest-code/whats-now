import { sleep } from '../lib'
import * as apiConf from '../lib/api/conf'

const awaitngUserModule = apiConf.getUserModule()

export default {
	namespaced: true,
	state() {
		return {
			userModule: null,
		}		
	},
	mutations: {
		setUserModule(state, userModule) {
			state.userModule = userModule
		}
	},
	actions: {
		async forceUpdateUserModule({ commit }) {
			commit('setUserModule', await apiConf.getUserModule())
		}
	},
	getters: {
		userModule: async (state) => {
			while (state.userModule === null) {
				await sleep(250)
			}

			return state.userModule
		}
	}
}

export const plugins = [
	async (store) => {
		store.commit('config/setUserModule', await awaitngUserModule)
	}
]