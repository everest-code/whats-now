import { createStore, createLogger } from 'vuex'
import clients, { plugins as clientsPlugins } from './clients'
import appointments, { plugins as appointmentsPlugins } from './appointments'
import config, { plugins as configPlugins } from './config'
import user, { plugins as userPlugins } from './user'

const store = createStore({
	strict: process.env.NODE_ENV !== 'production',
	modules: {
		clients,
		config,
		user,
		appointments
	},
	plugins: [
		...configPlugins,
		...clientsPlugins,
		...userPlugins,
		...appointmentsPlugins
	]
})

export default store
export const errorStore = createStore({
	state() {
		return {
			path: null,
			description: null,
			errorName: null
		}
	},
	mutations: {
		load(state, { path, description, name }) {
			state.path = path
			state.description = description
			state.errorName = name
		},
		clear(state) {
			state.path = null
			state.description = null
			state.errorName = null
		}
	},
	plugins: [
		createLogger()
	]
})