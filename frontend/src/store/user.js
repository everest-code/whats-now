import * as apiUsers from '../lib/api/users'
import { ref } from 'vue'

const storageKey = 'user'

const setUserKey = (key) => localStorage.setItem(storageKey, key.toString())
const removeUserKey = () => localStorage.removeItem(storageKey)
const getUserKey = () => {
	let value = localStorage.getItem(storageKey)
	if (value != null) {
		return parseInt(value)
	} else {
		return null
	}
}

export default {
	namespaced: true,
	state() {
		return {
			nit: getUserKey()
		}
	},
	mutations: {
		set(state, nit) {
			state.nit = nit
			setUserKey(nit)
		},
		logout(state) {
			state.nit = null
			
			removeUserKey()
		}
	},
	actions: {
		async login({ commit }, { nit, password }) {
			let data = await apiUsers.login({ nit, password })
			if (data == null) {
				return null
			}

			commit('set', nit)
			return data.fullName
		},
		async create(store, { nit, fullName, password }) {
			try {
				await apiUsers.create({ nit, fullName, password })
				return null
			} catch (err) {
				return err
			}
		}
	},
	getters: {
		loggedIn: (state) => state.nit != null,
	}
}

export const plugins = []