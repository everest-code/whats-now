module.exports = {
  productionSourceMap: false,
  publicPath: '/',
  pwa: {
    workboxPluginMode: 'InjectManifest',
    workboxOptions:{
      swSrc: "src/serviceWorker.js"
    },
    name: 'Whats Now',
    themeColor: '#2CA343',
    msTileColor: '#666F79',
    backgroundColor: '#FFFFFF',
    appleMobileWebAppStatusBarStyle: 'standalone',
    manifestCrossorigin: 'anonymous',
    appleMobileWebAppCapable: 'yes',
    appleMobileWebAppStatusBarStyle: 'black-translucent',
    iconPaths: {
      favicon16: 'favicon.16.ico',
      favicon32: 'favicon.ico',
      maskIcon: 'img/safari-mask.svg'
    },
    manifestOptions: {
      start_url: '/#/',
      lang: 'es',
      orientation: 'landscape',
      icons: [
        {
          src: '/img/icons/1024.png',
          sizes: '1024x1024',
          type: 'image/png'
        },
        {
          src: '/img/icons/512.png',
          sizes: '512x512',
          type: 'image/png'
        },
        {
          src: '/img/icons/256.png',
          sizes: '256x256',
          type: 'image/png'
        },
        {
          src: '/img/icons/128.png',
          sizes: '128x128',
          type: 'image/png'
        },
        {
          src: '/img/icons/64.png',
          sizes: '64x64',
          type: 'image/png'
        },
        {
          src: '/img/icons/32.png',
          sizes: '32x32',
          type: 'image/png'
        },
        {
          src: '/img/icons/16.png',
          sizes: '16x16',
          type: 'image/png'
        }
      ]
    }
  }
}